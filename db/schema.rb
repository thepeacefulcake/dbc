# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180807121922) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree
  end

  create_table "projects", force: :cascade do |t|
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "title"
    t.text     "description"
    t.string   "link"
    t.text     "credits"
    t.string   "brand"
    t.string   "slug"
    t.string   "link0"
    t.string   "credit1"
    t.string   "link1"
    t.string   "credit2"
    t.string   "link2"
    t.string   "credit3"
    t.string   "link3"
    t.string   "credit4"
    t.string   "link4"
    t.text     "description2"
    t.string   "projectimg_file_name"
    t.string   "projectimg_content_type"
    t.integer  "projectimg_file_size"
    t.datetime "projectimg_updated_at"
    t.string   "projectimg2_file_name"
    t.string   "projectimg2_content_type"
    t.integer  "projectimg2_file_size"
    t.datetime "projectimg2_updated_at"
    t.string   "link5"
    t.string   "credit5"
    t.string   "link6"
    t.string   "credit6"
    t.string   "link7"
    t.string   "credit7"
    t.string   "link8"
    t.string   "credit8"
    t.string   "link9"
    t.string   "credit9"
    t.string   "link10"
    t.string   "credit10"
    t.string   "category"
    t.index ["slug"], name: "index_projects_on_slug", unique: true, using: :btree
  end

end
