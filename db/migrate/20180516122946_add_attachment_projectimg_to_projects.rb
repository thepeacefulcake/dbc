class AddAttachmentProjectimgToProjects < ActiveRecord::Migration
  def self.up
    change_table :projects do |t|
      t.attachment :projectimg
    end
  end

  def self.down
    remove_attachment :projects, :projectimg
  end
end
