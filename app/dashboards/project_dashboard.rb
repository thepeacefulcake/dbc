	require "administrate/base_dashboard"

class ProjectDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    image_file_name: Field::String,
    image_content_type: Field::String,
    image_file_size: Field::Number,
    image_updated_at: Field::DateTime,
    image: PaperclipField,
    projectimg: PaperclipField,
    projectimg2: PaperclipField,
    title: Field::String,
    description: Field::Text,
    description2: Field::Text,
    link: Field::String,
    brand: Field::String,
    category: Field::String,
    credit1: Field::String,
    link1: Field::String,
    credit2: Field::String,
    link2: Field::String,
    credit3: Field::String,
    link3: Field::String,
    credit4: Field::String,
    link4: Field::String,
    credit5: Field::String,
    link5: Field::String,
    credit6: Field::String,
    link6: Field::String,
    credit7: Field::String,
    link7: Field::String,
    credit8: Field::String,
    link8: Field::String,
    credit9: Field::String,
    link9: Field::String,
    credit10: Field::String,
    link10: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :title,
    :created_at,
    :updated_at,
    :brand,
   
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :title,
    :description,
    :description2,
    :created_at,
    :updated_at,
    :link,
    :brand,
    :category,
    :credit1,
    :link1,
    :credit2,
    :link2,
    :credit3,
    :link3,
    :credit4,
    :link4, 
    :credit5,    
    :link5,
    :credit6,
    :link6,
    :credit7,
    :link7,
    :credit8,
    :link8,
    :credit9,
    :link9,
    :credit10,
    :link10,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :image,
    :projectimg,
    :projectimg2,
    :title,
    :description,
    :description2,
    :link,
    :brand,
    :category,
    :created_at,
    :updated_at,
    :credit1,
    :link1,
    :credit2,
    :link2,
    :credit3,
    :link3,
    :credit4,
    :link4,
    :credit5,
    :link5,
    :credit6,
    :link6,
    :credit7,
    :link7,
    :credit8,
    :link8,
    :credit9,
    :link9,
    :credit10,
    :link10,
  ].freeze

  # Overwrite this method to customize how projects are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(project)
  #   "Project ##{project.id}"
  # end
end


